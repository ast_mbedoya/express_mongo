'use strict'

const express = require('express')
const server = express()
const postsRouter = require('./routers/PostsRouter')

server.use((req, res, next) => {
    console.log('Incoming request...')
    next()
});
server.use('/posts', postsRouter)

server.listen(3000, () => console.log('listening...'))