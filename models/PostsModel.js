var MongoData = require('../data/MongoData');
let mongoCollectionName = 'posts'

let create = function () {
}

let getAll = function (callback) {
    MongoData.connect( (database, mongoClient) => {
        database.collection(mongoCollectionName).find({}).toArray((err, docs) => {
            callback(docs)
            mongoClient.close()
        })
    })
}

module.exports.create = create
module.exports.getAll = getAll