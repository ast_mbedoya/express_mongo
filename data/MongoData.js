let MongoClient = require('mongodb').MongoClient
const databaseName = 'posts'
const databaseServerUrl = 'mongodb://posts_user:mauricio12@ds153700.mlab.com:53700/' + databaseName

let connect = function (callback) {
    MongoClient.connect(databaseServerUrl, function (err, client) {
        if (err){
            console.log(err)
            throw err
        }
        callback(client.db(databaseName), client)
    })
}

module.exports.connect = connect