var express = require('express')
var router = express.Router()
var postsModel = require('../models/PostsModel')

router.use(function timeLog(req, res, next) {
    console.log('/Posts - Incoming request time: ', Date.now())
    next()
})

router.get('/', function (req, res) {
    postsModel.getAll( rows => {
        res.json(rows)
    })
})

router.get('/:postid', function (req, res) {
    res.json({
        id: req.params.postid,
        title: 'post' + req.params.postid
    })
})

module.exports = router